package com.Yeblo.Information.Entity;

public class FetchNextNumberResponse {

	    private Integer oldValue;
	    private Integer newValue;
	    
		public Integer getOldValue() {
			return oldValue;
		}
		public void setOldValue(Integer oldValue) {
			this.oldValue = oldValue;
		}
		public Integer getNewValue() {
			return newValue;
		}
		public void setNewValue(Integer newValue) {
			this.newValue = newValue;
		}

	    
	}




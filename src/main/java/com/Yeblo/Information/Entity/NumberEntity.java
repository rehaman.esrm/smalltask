package com.Yeblo.Information.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class NumberEntity {

	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long id;
	    
	    private String categoryCode;
	    
	    private Integer value;
	    
	    // getters and setters
	    
	    public Long getId() {
	        return id;
	    }
	    
	    public void setId(Long id) {
	        this.id = id;
	    }
	    
	    public String getCategoryCode() {
	        return categoryCode;
	    }
	    
	    public void setCategoryCode(String categoryCode) {
	        this.categoryCode = categoryCode;
	    }
	    
	    public int getValue() {
	        return value;
	    }
	    
	    public void setValue(int value) {
	        this.value = value;
	    }
	}




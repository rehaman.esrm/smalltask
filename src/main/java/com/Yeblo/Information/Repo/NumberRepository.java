package com.Yeblo.Information.Repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.Yeblo.Information.Entity.NumberEntity;

@Repository
	public interface NumberRepository extends JpaRepository<NumberEntity, Long> {
	    NumberEntity findByCategoryCode(String categoryCode);
	}



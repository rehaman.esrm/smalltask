package com.Yeblo.Information;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmallTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmallTaskApplication.class, args);
		System.out.println("applicationstarted");
	}

}

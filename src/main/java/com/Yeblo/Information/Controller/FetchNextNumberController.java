package com.Yeblo.Information.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.Yeblo.Information.Entity.FetchNextNumberRequest;
import com.Yeblo.Information.Entity.FetchNextNumberResponse;
import com.Yeblo.Information.Entity.NumberEntity;
import com.Yeblo.Information.Repo.NumberRepository;


	
	@RestController
	@RequestMapping("/FetchNextNumber")
	public class FetchNextNumberController {
		@Autowired
		private NumberRepository numberRepository;
	    @PostMapping("/")
	    public FetchNextNumberResponse fetchNextNumber(@RequestBody FetchNextNumberRequest request) {
	        // Fetch the value from the database using the category code
	        String categoryCode = request.getCategoryCode();

	    	NumberEntity numberEntity = numberRepository.findByCategoryCode(categoryCode);
	        int fetchedValue = numberEntity != null ? numberEntity.getValue() : 0;	        
	        // Calculate the next number based on the fetched value
	        int nextNumber = calculateNextNumber(fetchedValue);

	        if (numberEntity != null) {
	            numberEntity.setValue(nextNumber);
	            numberRepository.save(numberEntity);
	        } else {
	        	numberEntity = new NumberEntity();
	        	numberEntity.setCategoryCode(categoryCode);
	        	
	        	  numberEntity.setValue(nextNumber);
		            numberRepository.save(numberEntity);
	        }
	        // Delay for 5 seconds (you can use Thread.sleep(5000))

	        // Prepare the response
	        FetchNextNumberResponse response = new FetchNextNumberResponse();
	        response.setOldValue(fetchedValue);
	        response.setNewValue(nextNumber);

	        return response;
	    }

	    private int calculateNextNumber(int fetchedValue) {
	        int nextNumber = fetchedValue + 1;
	        while (sumOfDigits(nextNumber) != 1) {
	            nextNumber++;
	        }
	        return nextNumber;
	    }

	    private int sumOfDigits(int number) {
	        int sum = 0;
	        while (number != 0) {
	            sum += number % 10;
	            number /= 10;
	        }
	        return sum;
	    }
	}

	   
	
